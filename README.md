# react-native-apollo-list

apollo list

## Installation

```sh
npm install react-native-apollo-list
```

## Usage

```js
import ApolloList from "react-native-apollo-list";

// ...

const result = await ApolloList.multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
````
"husky": {
"hooks": {
"commit-msg": "commitlint -E HUSKY_GIT_PARAMS",
"pre-commit": "yarn lint && yarn typescript"
}
},
