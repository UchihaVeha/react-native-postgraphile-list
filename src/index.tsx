import React, { PropsWithChildren } from 'react';
import { FlatList, FlatListProps } from 'react-native';

type Props<T> = {} & FlatListProps<T>;

const InfinityList = <T extends unknown>({
  ...listProps
}: PropsWithChildren<Props<T>>) => {
  return <FlatList {...listProps} />;
};

export default InfinityList;
