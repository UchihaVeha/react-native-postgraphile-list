import * as React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { ApolloProvider } from '@apollo/client';
import client from './ApolloClient';
import TestList from './TestList';

export default function App() {
  return (
    <ApolloProvider client={client}>
      <View style={styles.container}>
        <TestList />
      </View>
    </ApolloProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
