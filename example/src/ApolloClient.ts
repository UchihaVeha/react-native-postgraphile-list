import { ApolloClient, HttpLink, InMemoryCache } from '@apollo/client';
import config from './config';

const httpLink = new HttpLink({
  uri: config.graphqlServerUrl,
});

const client = new ApolloClient({
  cache: new InMemoryCache({
    typePolicies: {
      Query: {
        fields: {
          // https://www.apollographql.com/docs/react/pagination/offset-based/
          allTestTables: {
            keyArgs: false,

            read(existing = {}, { args: { offset, limit } }) {
              return existing || undefined;
            },

            merge(existing = {}, incoming, { args: { offset = 0 } }) {
              const merged = existing?.nodes ? existing?.nodes.slice(0) : [];
              for (let i = 0; i < incoming.nodes.length; ++i) {
                merged[offset + i] = incoming.nodes[i];
              }
              return {
                ...existing,
                nodes: merged,
              };
            },
          },
        },
      },
    },
  }),
  link: httpLink,
});

export default client;
