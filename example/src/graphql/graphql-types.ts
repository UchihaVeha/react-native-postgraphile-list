import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions =  {}
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** A floating point number that requires more precision than IEEE 754 binary 64 */
  BigFloat: any;
  /**
   * A point in time as described by the [ISO
   * 8601](https://en.wikipedia.org/wiki/ISO_8601) standard. May or may not include a timezone.
   */
  Datetime: string;
  /** A location in a connection that can be used for resuming pagination. */
  Cursor: any;
};

/** The root query type which gives access points into the data universe. */
export type Query = Node & {
  __typename?: 'Query';
  /**
   * Exposes the root query type nested one level down. This is helpful for Relay 1
   * which can only query top level fields if they are in a particular form.
   */
  query: Query;
  /** The root query type must be a `Node` to work well with Relay 1 mutations. This just resolves to `query`. */
  nodeId: Scalars['ID'];
  /** Fetches an object given its globally unique `ID`. */
  node: Maybe<Node>;
  /** Reads and enables pagination through a set of `TestTable`. */
  allTestTables: Maybe<TestTablesConnection>;
  testTableById: Maybe<TestTable>;
  /** Reads a single `TestTable` using its globally unique `ID`. */
  testTable: Maybe<TestTable>;
};


/** The root query type which gives access points into the data universe. */
export type QueryNodeArgs = {
  nodeId: Scalars['ID'];
};


/** The root query type which gives access points into the data universe. */
export type QueryAllTestTablesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<TestTablesOrderBy>>;
  condition?: Maybe<TestTableCondition>;
  filter?: Maybe<TestTableFilter>;
};


/** The root query type which gives access points into the data universe. */
export type QueryTestTableByIdArgs = {
  id: Scalars['Int'];
};


/** The root query type which gives access points into the data universe. */
export type QueryTestTableArgs = {
  nodeId: Scalars['ID'];
};

/** An object with a globally unique `ID`. */
export type Node = {
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID'];
};

/** A connection to a list of `TestTable` values. */
export type TestTablesConnection = {
  __typename?: 'TestTablesConnection';
  /** A list of `TestTable` objects. */
  nodes: Array<Maybe<TestTable>>;
  /** A list of edges which contains the `TestTable` and cursor to aid in pagination. */
  edges: Array<TestTablesEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `TestTable` you could get from the connection. */
  totalCount: Scalars['Int'];
};

export type TestTable = Node & {
  __typename?: 'TestTable';
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID'];
  id: Scalars['Int'];
  text: Scalars['String'];
  decimal: Scalars['BigFloat'];
  integer: Scalars['Int'];
  boolean: Scalars['Boolean'];
  date: Scalars['Datetime'];
  createdAt: Scalars['Datetime'];
  updatedAt: Scalars['Datetime'];
};



/** A `TestTable` edge in the connection. */
export type TestTablesEdge = {
  __typename?: 'TestTablesEdge';
  /** A cursor for use in pagination. */
  cursor: Maybe<Scalars['Cursor']>;
  /** The `TestTable` at the end of the edge. */
  node: Maybe<TestTable>;
};


/** Information about pagination in a connection. */
export type PageInfo = {
  __typename?: 'PageInfo';
  /** When paginating forwards, are there more items? */
  hasNextPage: Scalars['Boolean'];
  /** When paginating backwards, are there more items? */
  hasPreviousPage: Scalars['Boolean'];
  /** When paginating backwards, the cursor to continue. */
  startCursor: Maybe<Scalars['Cursor']>;
  /** When paginating forwards, the cursor to continue. */
  endCursor: Maybe<Scalars['Cursor']>;
};

/** Methods to use when ordering `TestTable`. */
export enum TestTablesOrderBy {
  Natural = 'NATURAL',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  TextAsc = 'TEXT_ASC',
  TextDesc = 'TEXT_DESC',
  DecimalAsc = 'DECIMAL_ASC',
  DecimalDesc = 'DECIMAL_DESC',
  IntegerAsc = 'INTEGER_ASC',
  IntegerDesc = 'INTEGER_DESC',
  BooleanAsc = 'BOOLEAN_ASC',
  BooleanDesc = 'BOOLEAN_DESC',
  DateAsc = 'DATE_ASC',
  DateDesc = 'DATE_DESC',
  CreatedAtAsc = 'CREATED_AT_ASC',
  CreatedAtDesc = 'CREATED_AT_DESC',
  UpdatedAtAsc = 'UPDATED_AT_ASC',
  UpdatedAtDesc = 'UPDATED_AT_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC'
}

/**
 * A condition to be used against `TestTable` object types. All fields are tested
 * for equality and combined with a logical ‘and.’
 */
export type TestTableCondition = {
  /** Checks for equality with the object’s `id` field. */
  id?: Maybe<Scalars['Int']>;
  /** Checks for equality with the object’s `text` field. */
  text?: Maybe<Scalars['String']>;
  /** Checks for equality with the object’s `decimal` field. */
  decimal?: Maybe<Scalars['BigFloat']>;
  /** Checks for equality with the object’s `integer` field. */
  integer?: Maybe<Scalars['Int']>;
  /** Checks for equality with the object’s `boolean` field. */
  boolean?: Maybe<Scalars['Boolean']>;
  /** Checks for equality with the object’s `date` field. */
  date?: Maybe<Scalars['Datetime']>;
  /** Checks for equality with the object’s `createdAt` field. */
  createdAt?: Maybe<Scalars['Datetime']>;
  /** Checks for equality with the object’s `updatedAt` field. */
  updatedAt?: Maybe<Scalars['Datetime']>;
};

/** A filter to be used against `TestTable` object types. All fields are combined with a logical ‘and.’ */
export type TestTableFilter = {
  /** Filter by the object’s `id` field. */
  id?: Maybe<IntFilter>;
  /** Filter by the object’s `text` field. */
  text?: Maybe<StringFilter>;
  /** Filter by the object’s `decimal` field. */
  decimal?: Maybe<BigFloatFilter>;
  /** Filter by the object’s `integer` field. */
  integer?: Maybe<IntFilter>;
  /** Filter by the object’s `boolean` field. */
  boolean?: Maybe<BooleanFilter>;
  /** Filter by the object’s `date` field. */
  date?: Maybe<DatetimeFilter>;
  /** Filter by the object’s `createdAt` field. */
  createdAt?: Maybe<DatetimeFilter>;
  /** Filter by the object’s `updatedAt` field. */
  updatedAt?: Maybe<DatetimeFilter>;
  /** Checks for all expressions in this list. */
  and?: Maybe<Array<TestTableFilter>>;
  /** Checks for any expressions in this list. */
  or?: Maybe<Array<TestTableFilter>>;
  /** Negates the expression. */
  not?: Maybe<TestTableFilter>;
};

/** A filter to be used against Int fields. All fields are combined with a logical ‘and.’ */
export type IntFilter = {
  /** Is null (if `true` is specified) or is not null (if `false` is specified). */
  isNull?: Maybe<Scalars['Boolean']>;
  /** Equal to the specified value. */
  equalTo?: Maybe<Scalars['Int']>;
  /** Not equal to the specified value. */
  notEqualTo?: Maybe<Scalars['Int']>;
  /** Not equal to the specified value, treating null like an ordinary value. */
  distinctFrom?: Maybe<Scalars['Int']>;
  /** Equal to the specified value, treating null like an ordinary value. */
  notDistinctFrom?: Maybe<Scalars['Int']>;
  /** Included in the specified list. */
  in?: Maybe<Array<Scalars['Int']>>;
  /** Not included in the specified list. */
  notIn?: Maybe<Array<Scalars['Int']>>;
  /** Less than the specified value. */
  lessThan?: Maybe<Scalars['Int']>;
  /** Less than or equal to the specified value. */
  lessThanOrEqualTo?: Maybe<Scalars['Int']>;
  /** Greater than the specified value. */
  greaterThan?: Maybe<Scalars['Int']>;
  /** Greater than or equal to the specified value. */
  greaterThanOrEqualTo?: Maybe<Scalars['Int']>;
};

/** A filter to be used against String fields. All fields are combined with a logical ‘and.’ */
export type StringFilter = {
  /** Is null (if `true` is specified) or is not null (if `false` is specified). */
  isNull?: Maybe<Scalars['Boolean']>;
  /** Equal to the specified value. */
  equalTo?: Maybe<Scalars['String']>;
  /** Not equal to the specified value. */
  notEqualTo?: Maybe<Scalars['String']>;
  /** Not equal to the specified value, treating null like an ordinary value. */
  distinctFrom?: Maybe<Scalars['String']>;
  /** Equal to the specified value, treating null like an ordinary value. */
  notDistinctFrom?: Maybe<Scalars['String']>;
  /** Included in the specified list. */
  in?: Maybe<Array<Scalars['String']>>;
  /** Not included in the specified list. */
  notIn?: Maybe<Array<Scalars['String']>>;
  /** Less than the specified value. */
  lessThan?: Maybe<Scalars['String']>;
  /** Less than or equal to the specified value. */
  lessThanOrEqualTo?: Maybe<Scalars['String']>;
  /** Greater than the specified value. */
  greaterThan?: Maybe<Scalars['String']>;
  /** Greater than or equal to the specified value. */
  greaterThanOrEqualTo?: Maybe<Scalars['String']>;
  /** Contains the specified string (case-sensitive). */
  includes?: Maybe<Scalars['String']>;
  /** Does not contain the specified string (case-sensitive). */
  notIncludes?: Maybe<Scalars['String']>;
  /** Contains the specified string (case-insensitive). */
  includesInsensitive?: Maybe<Scalars['String']>;
  /** Does not contain the specified string (case-insensitive). */
  notIncludesInsensitive?: Maybe<Scalars['String']>;
  /** Starts with the specified string (case-sensitive). */
  startsWith?: Maybe<Scalars['String']>;
  /** Does not start with the specified string (case-sensitive). */
  notStartsWith?: Maybe<Scalars['String']>;
  /** Starts with the specified string (case-insensitive). */
  startsWithInsensitive?: Maybe<Scalars['String']>;
  /** Does not start with the specified string (case-insensitive). */
  notStartsWithInsensitive?: Maybe<Scalars['String']>;
  /** Ends with the specified string (case-sensitive). */
  endsWith?: Maybe<Scalars['String']>;
  /** Does not end with the specified string (case-sensitive). */
  notEndsWith?: Maybe<Scalars['String']>;
  /** Ends with the specified string (case-insensitive). */
  endsWithInsensitive?: Maybe<Scalars['String']>;
  /** Does not end with the specified string (case-insensitive). */
  notEndsWithInsensitive?: Maybe<Scalars['String']>;
  /** Matches the specified pattern (case-sensitive). An underscore (_) matches any single character; a percent sign (%) matches any sequence of zero or more characters. */
  like?: Maybe<Scalars['String']>;
  /** Does not match the specified pattern (case-sensitive). An underscore (_) matches any single character; a percent sign (%) matches any sequence of zero or more characters. */
  notLike?: Maybe<Scalars['String']>;
  /** Matches the specified pattern (case-insensitive). An underscore (_) matches any single character; a percent sign (%) matches any sequence of zero or more characters. */
  likeInsensitive?: Maybe<Scalars['String']>;
  /** Does not match the specified pattern (case-insensitive). An underscore (_) matches any single character; a percent sign (%) matches any sequence of zero or more characters. */
  notLikeInsensitive?: Maybe<Scalars['String']>;
  /** Equal to the specified value (case-insensitive). */
  equalToInsensitive?: Maybe<Scalars['String']>;
  /** Not equal to the specified value (case-insensitive). */
  notEqualToInsensitive?: Maybe<Scalars['String']>;
  /** Not equal to the specified value, treating null like an ordinary value (case-insensitive). */
  distinctFromInsensitive?: Maybe<Scalars['String']>;
  /** Equal to the specified value, treating null like an ordinary value (case-insensitive). */
  notDistinctFromInsensitive?: Maybe<Scalars['String']>;
  /** Included in the specified list (case-insensitive). */
  inInsensitive?: Maybe<Array<Scalars['String']>>;
  /** Not included in the specified list (case-insensitive). */
  notInInsensitive?: Maybe<Array<Scalars['String']>>;
  /** Less than the specified value (case-insensitive). */
  lessThanInsensitive?: Maybe<Scalars['String']>;
  /** Less than or equal to the specified value (case-insensitive). */
  lessThanOrEqualToInsensitive?: Maybe<Scalars['String']>;
  /** Greater than the specified value (case-insensitive). */
  greaterThanInsensitive?: Maybe<Scalars['String']>;
  /** Greater than or equal to the specified value (case-insensitive). */
  greaterThanOrEqualToInsensitive?: Maybe<Scalars['String']>;
};

/** A filter to be used against BigFloat fields. All fields are combined with a logical ‘and.’ */
export type BigFloatFilter = {
  /** Is null (if `true` is specified) or is not null (if `false` is specified). */
  isNull?: Maybe<Scalars['Boolean']>;
  /** Equal to the specified value. */
  equalTo?: Maybe<Scalars['BigFloat']>;
  /** Not equal to the specified value. */
  notEqualTo?: Maybe<Scalars['BigFloat']>;
  /** Not equal to the specified value, treating null like an ordinary value. */
  distinctFrom?: Maybe<Scalars['BigFloat']>;
  /** Equal to the specified value, treating null like an ordinary value. */
  notDistinctFrom?: Maybe<Scalars['BigFloat']>;
  /** Included in the specified list. */
  in?: Maybe<Array<Scalars['BigFloat']>>;
  /** Not included in the specified list. */
  notIn?: Maybe<Array<Scalars['BigFloat']>>;
  /** Less than the specified value. */
  lessThan?: Maybe<Scalars['BigFloat']>;
  /** Less than or equal to the specified value. */
  lessThanOrEqualTo?: Maybe<Scalars['BigFloat']>;
  /** Greater than the specified value. */
  greaterThan?: Maybe<Scalars['BigFloat']>;
  /** Greater than or equal to the specified value. */
  greaterThanOrEqualTo?: Maybe<Scalars['BigFloat']>;
};

/** A filter to be used against Boolean fields. All fields are combined with a logical ‘and.’ */
export type BooleanFilter = {
  /** Is null (if `true` is specified) or is not null (if `false` is specified). */
  isNull?: Maybe<Scalars['Boolean']>;
  /** Equal to the specified value. */
  equalTo?: Maybe<Scalars['Boolean']>;
  /** Not equal to the specified value. */
  notEqualTo?: Maybe<Scalars['Boolean']>;
  /** Not equal to the specified value, treating null like an ordinary value. */
  distinctFrom?: Maybe<Scalars['Boolean']>;
  /** Equal to the specified value, treating null like an ordinary value. */
  notDistinctFrom?: Maybe<Scalars['Boolean']>;
  /** Included in the specified list. */
  in?: Maybe<Array<Scalars['Boolean']>>;
  /** Not included in the specified list. */
  notIn?: Maybe<Array<Scalars['Boolean']>>;
  /** Less than the specified value. */
  lessThan?: Maybe<Scalars['Boolean']>;
  /** Less than or equal to the specified value. */
  lessThanOrEqualTo?: Maybe<Scalars['Boolean']>;
  /** Greater than the specified value. */
  greaterThan?: Maybe<Scalars['Boolean']>;
  /** Greater than or equal to the specified value. */
  greaterThanOrEqualTo?: Maybe<Scalars['Boolean']>;
};

/** A filter to be used against Datetime fields. All fields are combined with a logical ‘and.’ */
export type DatetimeFilter = {
  /** Is null (if `true` is specified) or is not null (if `false` is specified). */
  isNull?: Maybe<Scalars['Boolean']>;
  /** Equal to the specified value. */
  equalTo?: Maybe<Scalars['Datetime']>;
  /** Not equal to the specified value. */
  notEqualTo?: Maybe<Scalars['Datetime']>;
  /** Not equal to the specified value, treating null like an ordinary value. */
  distinctFrom?: Maybe<Scalars['Datetime']>;
  /** Equal to the specified value, treating null like an ordinary value. */
  notDistinctFrom?: Maybe<Scalars['Datetime']>;
  /** Included in the specified list. */
  in?: Maybe<Array<Scalars['Datetime']>>;
  /** Not included in the specified list. */
  notIn?: Maybe<Array<Scalars['Datetime']>>;
  /** Less than the specified value. */
  lessThan?: Maybe<Scalars['Datetime']>;
  /** Less than or equal to the specified value. */
  lessThanOrEqualTo?: Maybe<Scalars['Datetime']>;
  /** Greater than the specified value. */
  greaterThan?: Maybe<Scalars['Datetime']>;
  /** Greater than or equal to the specified value. */
  greaterThanOrEqualTo?: Maybe<Scalars['Datetime']>;
};

/** The root mutation type which contains root level fields which mutate data. */
export type Mutation = {
  __typename?: 'Mutation';
  /** Creates a single `TestTable`. */
  createTestTable: Maybe<CreateTestTablePayload>;
  /** Updates a single `TestTable` using its globally unique id and a patch. */
  updateTestTable: Maybe<UpdateTestTablePayload>;
  /** Updates a single `TestTable` using a unique key and a patch. */
  updateTestTableById: Maybe<UpdateTestTablePayload>;
  /** Deletes a single `TestTable` using its globally unique id. */
  deleteTestTable: Maybe<DeleteTestTablePayload>;
  /** Deletes a single `TestTable` using a unique key. */
  deleteTestTableById: Maybe<DeleteTestTablePayload>;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateTestTableArgs = {
  input: CreateTestTableInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateTestTableArgs = {
  input: UpdateTestTableInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateTestTableByIdArgs = {
  input: UpdateTestTableByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteTestTableArgs = {
  input: DeleteTestTableInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteTestTableByIdArgs = {
  input: DeleteTestTableByIdInput;
};

/** The output of our create `TestTable` mutation. */
export type CreateTestTablePayload = {
  __typename?: 'CreateTestTablePayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId: Maybe<Scalars['String']>;
  /** The `TestTable` that was created by this mutation. */
  testTable: Maybe<TestTable>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query: Maybe<Query>;
  /** An edge for our `TestTable`. May be used by Relay 1. */
  testTableEdge: Maybe<TestTablesEdge>;
};


/** The output of our create `TestTable` mutation. */
export type CreateTestTablePayloadTestTableEdgeArgs = {
  orderBy?: Maybe<Array<TestTablesOrderBy>>;
};

/** All input for the create `TestTable` mutation. */
export type CreateTestTableInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `TestTable` to be created by this mutation. */
  testTable: TestTableInput;
};

/** An input for mutations affecting `TestTable` */
export type TestTableInput = {
  id?: Maybe<Scalars['Int']>;
  text?: Maybe<Scalars['String']>;
  decimal?: Maybe<Scalars['BigFloat']>;
  integer?: Maybe<Scalars['Int']>;
  boolean?: Maybe<Scalars['Boolean']>;
  date?: Maybe<Scalars['Datetime']>;
  createdAt?: Maybe<Scalars['Datetime']>;
  updatedAt?: Maybe<Scalars['Datetime']>;
};

/** The output of our update `TestTable` mutation. */
export type UpdateTestTablePayload = {
  __typename?: 'UpdateTestTablePayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId: Maybe<Scalars['String']>;
  /** The `TestTable` that was updated by this mutation. */
  testTable: Maybe<TestTable>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query: Maybe<Query>;
  /** An edge for our `TestTable`. May be used by Relay 1. */
  testTableEdge: Maybe<TestTablesEdge>;
};


/** The output of our update `TestTable` mutation. */
export type UpdateTestTablePayloadTestTableEdgeArgs = {
  orderBy?: Maybe<Array<TestTablesOrderBy>>;
};

/** All input for the `updateTestTable` mutation. */
export type UpdateTestTableInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `TestTable` to be updated. */
  nodeId: Scalars['ID'];
  /** An object where the defined keys will be set on the `TestTable` being updated. */
  testTablePatch: TestTablePatch;
};

/** Represents an update to a `TestTable`. Fields that are set will be updated. */
export type TestTablePatch = {
  id?: Maybe<Scalars['Int']>;
  text?: Maybe<Scalars['String']>;
  decimal?: Maybe<Scalars['BigFloat']>;
  integer?: Maybe<Scalars['Int']>;
  boolean?: Maybe<Scalars['Boolean']>;
  date?: Maybe<Scalars['Datetime']>;
  createdAt?: Maybe<Scalars['Datetime']>;
  updatedAt?: Maybe<Scalars['Datetime']>;
};

/** All input for the `updateTestTableById` mutation. */
export type UpdateTestTableByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** An object where the defined keys will be set on the `TestTable` being updated. */
  testTablePatch: TestTablePatch;
  id: Scalars['Int'];
};

/** The output of our delete `TestTable` mutation. */
export type DeleteTestTablePayload = {
  __typename?: 'DeleteTestTablePayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId: Maybe<Scalars['String']>;
  /** The `TestTable` that was deleted by this mutation. */
  testTable: Maybe<TestTable>;
  deletedTestTableId: Maybe<Scalars['ID']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query: Maybe<Query>;
  /** An edge for our `TestTable`. May be used by Relay 1. */
  testTableEdge: Maybe<TestTablesEdge>;
};


/** The output of our delete `TestTable` mutation. */
export type DeleteTestTablePayloadTestTableEdgeArgs = {
  orderBy?: Maybe<Array<TestTablesOrderBy>>;
};

/** All input for the `deleteTestTable` mutation. */
export type DeleteTestTableInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `TestTable` to be deleted. */
  nodeId: Scalars['ID'];
};

/** All input for the `deleteTestTableById` mutation. */
export type DeleteTestTableByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
};

export type TestDataQueryVariables = Exact<{
  offset: Scalars['Int'];
  limit: Scalars['Int'];
}>;


export type TestDataQuery = (
  { __typename?: 'Query' }
  & { allTestTables: Maybe<(
    { __typename?: 'TestTablesConnection' }
    & { nodes: Array<Maybe<(
      { __typename?: 'TestTable' }
      & Pick<TestTable, 'id'>
    )>> }
  )> }
);


export const TestDataDocument = gql`
    query TestData($offset: Int!, $limit: Int!) {
  allTestTables(offset: $offset, first: $limit) {
    nodes {
      id
    }
  }
}
    `;

/**
 * __useTestDataQuery__
 *
 * To run a query within a React component, call `useTestDataQuery` and pass it any options that fit your needs.
 * When your component renders, `useTestDataQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useTestDataQuery({
 *   variables: {
 *      offset: // value for 'offset'
 *      limit: // value for 'limit'
 *   },
 * });
 */
export function useTestDataQuery(baseOptions: Apollo.QueryHookOptions<TestDataQuery, TestDataQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<TestDataQuery, TestDataQueryVariables>(TestDataDocument, options);
      }
export function useTestDataLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<TestDataQuery, TestDataQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<TestDataQuery, TestDataQueryVariables>(TestDataDocument, options);
        }
export type TestDataQueryHookResult = ReturnType<typeof useTestDataQuery>;
export type TestDataLazyQueryHookResult = ReturnType<typeof useTestDataLazyQuery>;
export type TestDataQueryResult = Apollo.QueryResult<TestDataQuery, TestDataQueryVariables>;

      export interface PossibleTypesResultData {
        possibleTypes: {
          [key: string]: string[]
        }
      }
      const result: PossibleTypesResultData = {
  "possibleTypes": {
    "Node": [
      "Query",
      "TestTable"
    ]
  }
};
      export default result;
    