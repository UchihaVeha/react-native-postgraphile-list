import React, { useState } from 'react';
import { View, Text, FlatList, Image } from 'react-native';
// @ts-ignore
import InfinityList from 'react-native-apollo-list';
import { useTestDataQuery, TestTable } from './graphql/graphql-types';

const TestList = () => {
  const [limit, setLimit] = useState(10);
  const { data, fetchMore } = useTestDataQuery({
    variables: {
      offset: 0,
      limit,
    },
  });

  const handleEndReached = async () => {
    const currentLength = data.allTestTables.nodes.length;
    try {
      if (fetchMore) {
        console.log({
          variables: { offset: currentLength, limit: 10 },
        });
        const result = await fetchMore({
          variables: { offset: currentLength, limit: 10 },
        });
        setLimit(result.data.allTestTables.nodes.length);
      }
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <View style={{ flex: 1, borderWidth: 1 }}>
      <InfinityList<TestTable>
        contentContainerStyle={{
          backgroundColor: '#FBFBF8',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 15,
        }}
        data={data?.allTestTables?.nodes ?? []}
        keyExtractor={(item) => item.id.toString()}
        onEndReached={handleEndReached}
        onEndReachedThreshold={0.5}
        initialNumToRender={10}
        renderItem={({ item }) => (
          <View
            style={{
              width: '100%',
              height: 80,
              marginTop: 10,
            }}
          >
            <View>
              <Text>{item.id}</Text>
            </View>
          </View>
        )}
      />
    </View>
  );
};

export default TestList;
