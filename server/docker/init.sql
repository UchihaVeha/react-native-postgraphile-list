
CREATE ROLE app_owner WITH LOGIN PASSWORD 'password';

grant all on schema public to app_owner;

CREATE DATABASE app OWNER app_owner;
CREATE DATABASE app_shadow OWNER app_owner;

grant all on database app to app_owner;

