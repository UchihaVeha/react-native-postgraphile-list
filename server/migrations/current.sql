
DROP TABLE IF EXISTS test_table;

CREATE TABLE public.test_table (
    id serial  PRIMARY KEY,
    text text NOT NULL DEFAULT '',
    decimal decimal NOT NULL DEFAULT 0,
    integer integer NOT NULL DEFAULT 0,
    boolean boolean NOT NULL DEFAULT false,
    date timestamptz NOT NULL DEFAULT now(),
    created_at timestamptz NOT NULL DEFAULT now(),
    updated_at timestamptz NOT NULL DEFAULT now()
);

INSERT INTO test_table (text, decimal, integer, boolean, date)
SELECT
    left(md5(i::text), 10),
    random() + random() * 100,
    random() * 100,
    random() > 0.5,
    NOW() + (random() * (NOW()+'90 days' - NOW())) + '30 days'
from generate_series(1, 100) s(i)
