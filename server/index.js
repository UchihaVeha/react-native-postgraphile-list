const express = require('express');
const cors = require('cors');
const { postgraphile } = require('postgraphile');
const graphqlConfig = require('./graphqlConfig');

const app = express();
app.use(cors());

const pG = postgraphile(process.env.DATABASE_URL, 'public', graphqlConfig);
app.use(function (req, res, next) {
  setTimeout(next, 2000);
});
app.use(pG);

app.listen(3007, function () {
  console.log('Dev server listening on port 3007!');
});
